package com.yue.ar.suite;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.StretchViewport;

public class App extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;

	BitmapFont font_ch;
	FreeTypeFontGenerator generator_ch;
	FreeTypeFontGenerator.FreeTypeFontParameter parameter_ch;
	// 标签控件
	private Label label;
	String strShowText = "Brad 恭賀新禧 !@#$%^&()_+=-`~*/.<>;':{}[]| 1234567890 "; // 造字檔

	// 舞台
	private Stage stage;



	// 视口世界的宽高统使用 480 * 800, 并统一使用伸展视口（StretchViewport）
	public static final float WORLD_WIDTH = 480;
	public static final float WORLD_HEIGHT = 800;


	@Override
	public void create () {
		batch = new SpriteBatch();

		// 使用伸展视口（StretchViewport）创建舞台
		stage = new Stage(new StretchViewport(WORLD_WIDTH, WORLD_HEIGHT));

		/*
         * 第 1 步: 创建 BitmapFont
         */
		// 读取 bitmapfont.fnt 文件创建位图字体
		generator_ch = new FreeTypeFontGenerator(Gdx.files.internal("font/DroidSansFallback.ttf"));
		parameter_ch = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter_ch.size=24;
		parameter_ch.characters = strShowText;
		font_ch = generator_ch.generateFont(parameter_ch);
		generator_ch.dispose();


		 /*
         * 第 2 步: 创建 LabelStyle
         */
		// 要创建 Label 首先要创建一个 Label 的样式, 用于指明 Label 所使用的位图字体, 背景图片, 颜色等
		Label.LabelStyle style = new Label.LabelStyle();


// 指定 Label 的背景, 可用纹理区域 textureRegion（在这里背景我就不再设置）
		// style.background = new TextureRegionDrawable(textureRegion);

		// 指定 Label 所使用的位图字体
		style.font = font_ch;

		// 指定 Label 字体的 RGBA 颜色, 在这里我设置为红色
		style.fontColor = new Color(0, 1, 1, 1);

        /*
         * 第 3 步: 创建 Label
         */
		// 根据 Label 的样式创建 Label, 第一个参数表示显示的文本（要显示的文本字符必须在 BitmapFont 中存在）
		label = new Label(strShowText, style);

		// 也可以通过方法设置或获取文本
		// label.setText("Hello");
		// String text = label.getText().toString();

		// 设置 Label 的显示位置
		label.setPosition(50, 400);

		// 可以通过设置字体的缩放比来控制字体显示的大小
		label.setFontScale(1.5f);

        /*
         * 第 4 步: 添加到舞台
         */
		// 添加 label 到舞台
		stage.addActor(label);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		// 更新舞台逻辑
		stage.act();
		// 绘制舞台
		stage.draw();
	}
	
	@Override
	public void dispose () {
		// 应用退出时释放资源
		if (font_ch != null) {
			font_ch.dispose();
		}
		if (stage != null) {
			stage.dispose();
		}
	}
}
